import '../css/main.scss';
import { initData } from './helpers/initData';
import { isTablet, isIE } from './helpers/domLogic';
import { configureNavigation } from './helpers/navigation';

document.addEventListener("DOMContentLoaded", function(event) {
    initData();

    //Configuración de navegación/carrusel en versión móvil
    if ( isTablet() ) {
        configureNavigation();
    }

    //Añadimos clase específica en el body cuando estemos en IE (para usar en nuestra hoja de estilos)
    if (isIE()) {
        document.body.classList.add("ie");
    }

    /* Mobile Menu */
    var menuIcon = document.getElementById("menuIcon")
    var menuLinks = document.getElementById("menuLinks")
    menuIcon.addEventListener("click", function(e){
        e.preventDefault();
        if ( menuLinks.classList.contains("toggle") ) {
            menuLinks.classList.remove("toggle")
            this.children[0].setAttribute("src","https://www.ecestaticos.com/file/70fee14b9671f64c5d843dd23c3182e6/1601021669-icon-menu.svg")
            setTimeout(function(){
                menuIcon.parentElement.classList.remove("menu--active")
            },600)
        } else {
            menuLinks.classList.add("toggle")
            this.children[0].setAttribute("src","https://www.ecestaticos.com/file/7eb789dff5625a6aa27be3224d1743a9/1601023389-icon-close.svg")
            this.parentElement.classList.add("menu--active")
        }
        //getOutTooltips();
    });
    
    //Actualización
    setInterval(function() {
        window.location.href = window.location.href;
    }, 120000); //Refresco cada dos minutos    
});