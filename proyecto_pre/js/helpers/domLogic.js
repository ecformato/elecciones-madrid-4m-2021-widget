//// URL

function getURLParam() {
    let response = 'covid';
    let parametros = window.location.search;

    if(parametros){
        const urlParametro = new URLSearchParams(parametros);
        let module = urlParametro.get('modulo');
        response = module == 'panel' ? 'panel' : 'covid'; 
    } 
    
    return response;
}

//// DOM

function drawCovidFirst() {
    document.getElementsByClassName('container')[0].classList.add('covid');
    widgetElement.classList.add('widget--covid');
    tabCovid.classList.add('tab--first', 'tab--active');
    tabPanel.classList.add('tab--last');
}

function drawPanelFirst() {
    document.getElementsByClassName('container')[0].classList.add('panel');
    widgetElement.classList.add('widget--panel');
    tabPanel.classList.add('tab--first', 'tab--active');
    tabCovid.classList.add('tab--last');
}

function applyCustomOrder(arr, desiredOrder) {
    let aux = [...arr]; //Así evitamos la mutación del array original y que afecte a otros módulos del widget
    const orderForIndexVals = desiredOrder.slice(0).reverse();
    aux.sort((a, b) => {
        const aIndex = -orderForIndexVals.indexOf(a.res_party.par_meta_id);
        const bIndex = -orderForIndexVals.indexOf(b.res_party.par_meta_id);
        return aIndex - bIndex;
    });

    return aux;
}

function ordenacionTabla(a,b) {
    const datoA = a.res_members;
    const datoB = b.res_members;

    let comparacion = 0;
    if (datoA > datoB) {
        comparacion = -1;
    } else if (datoA <= datoB) {
        comparacion = 1;
    }
    return comparacion;
}

/// Helpers
function getNumberWithThousandsPoint(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function isTablet() {
    return window.innerWidth <= 900
}

function isMobile() {
    return window.innerWidth < 485
}

function isIE() {
    let ua = window.navigator.userAgent;
    let msie = ua.indexOf("MSIE ");
    return (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
}

function whichViewportWidth() {
    let width = window.innerWidth;
    let result = '';

    if(width > 995){
        result = "desktop";
    } else if (width > 900 && width <= 995) {
        result = "mediumDesktop";
    } else if (width <= 767) {
        result = 'mobile';
    } else {
        result = 'tablet';
    }

    return result;
}

export {
    getURLParam,
    drawCovidFirst,
    drawPanelFirst,
    getNumberWithThousandsPoint,
    isTablet,
    isMobile,
    isIE,
    whichViewportWidth,
    applyCustomOrder,
    ordenacionTabla
}