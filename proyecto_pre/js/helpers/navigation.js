let currentVisibleSlide = 1;
let columns = document.getElementById("widgetColumns");
let columnsWidth;
let arrowPrev = document.getElementById("arrowPrev");
let arrowNext = document.getElementById("arrowNext");
let dots = document.getElementsByClassName("dot");

export function configureNavigation() {
	columnsWidth = columns.offsetWidth;
	addArrowsListeners();
}

function addArrowsListeners() {
	let arrows = document.getElementsByClassName("arrow");
	for (let i=0; i < arrows.length; i++) {
		arrows[i].addEventListener("click", function(e) {
			e.preventDefault();
			let direction = arrows[i].classList.contains("arrow--prev") ? "prev" : "next";
			if ( ( (direction == "prev") && (currentVisibleSlide != 1) ) || ( (direction == "next") && (currentVisibleSlide != 2) ) ) {
				updateSlide(direction);
			}
		});
	}
}

function updateSlide(dir) {
	currentVisibleSlide = ( dir == "prev" ) ? currentVisibleSlide - 1 : currentVisibleSlide + 1;
	let translation = columnsWidth * ( currentVisibleSlide - 1 );
	columns.style.transform = `translateX(-${translation}px)`;
	updateArrows();
	updateDots();
}

function updateArrows() {
	if ( currentVisibleSlide == 1 ) {
		arrowPrev.classList.add("arrow--disabled");
		arrowNext.classList.remove("arrow--disabled");
	} else {
		arrowPrev.classList.remove("arrow--disabled");
		arrowNext.classList.add("arrow--disabled");
	}
}

function updateDots() {
	if ( currentVisibleSlide == 1 ) {
		dots[0].classList.add("dot--active");
		dots[1].classList.remove("dot--active");
	} else {
		dots[0].classList.remove("dot--active");
		dots[1].classList.add("dot--active");
	}
}