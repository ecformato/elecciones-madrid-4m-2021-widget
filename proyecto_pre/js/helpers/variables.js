const CURRENTELECTION_ID = 100; //2021
const FORMERELECTION_ID = 96; //2019

export const apis = [
    `https://api.elconfidencial.com/service/electionsscytl/place/${CURRENTELECTION_ID}/CA13/?compareTo=${FORMERELECTION_ID}`, //DATOS AUTONÓMICOS > UTILIZAR EL COMPARETO
    `https://api.elconfidencial.com/service/electionsscytl/map/${CURRENTELECTION_ID}/5/CA13/` //Cambiar por datos municipales
]

export const backUpApis = [
    `https://api.elconfidencial.com/service/electionsscytl/place/${CURRENTELECTION_ID}/CA13/?compareTo=${FORMERELECTION_ID}`, //DATOS AUTONÓMICOS > UTILIZAR EL COMPARETO
    `https://api.elconfidencial.com/service/electionsscytl/map/${CURRENTELECTION_ID}/5/CA13/` //Cambiar por datos municipales
]

export const superPartyBlockId = {
    //Partidos con opciones de sacar escaño
    10: {alias: 'CS', largeAlias: 'CS', color: '#f45a09', bloque: 'derecha', cssClass: 'table__partido--cs'}, //Cs
    1000091: {alias: 'M. Madrid', largeAlias: 'Más Madrid', color: '#20c0b2', bloque: 'izquierda', cssClass: 'table__partido--masmadrid'}, //MM
    38: {alias: 'PP', largeAlias: 'PP', color: '#48aafd', bloque: 'derecha', cssClass: 'table__partido--pp'}, //PP
    40: {alias: 'PSOE', largeAlias: 'PSOE', color: '#f60b01', bloque: 'izquierda', cssClass: 'table__partido--psoe'}, //PSC
    64: {alias: 'Podemos', largeAlias: 'Podemos', color: '#692772', bloque: 'izquierda', cssClass: 'table__partido--podemos'}, //Podemos > En CAT, 80
    52: {alias: 'VOX', largeAlias: 'VOX', color: '#77be2d', bloque: 'derecha', cssClass: 'table__partido--vox'}, //VOX
    //Resto de candidaturas
    29: {alias: 'PACMA', largeAlias: 'PACMA', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    65: {alias: 'PUM+J', largeAlias: 'PUM+J', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    1000093: {alias: 'PCTE', largeAlias: 'PCTE', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    12: {alias: 'EB', largeAlias: 'Escaños en Blanco', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    4100005: {alias: 'UEP', largeAlias: 'Unión Europea de Pensionistas', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    19: {alias: 'FE de las JONS', largeAlias: 'FE de las JONS', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    34: {alias: 'P-LIB', largeAlias: 'P. Libertario', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    33: {alias: 'PH', largeAlias: 'P. Humanista', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    1000092: {alias: 'PCAS-TC', largeAlias: 'PCAS-TC', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    724: {alias: '3ª Acción', largeAlias: '3ª en Acción', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    4100012: {alias: 'RC-PCAS-TC', largeAlias: 'Recortes Cero-PCAS-TC-GV-M', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    4100010: {alias: 'PCOE-PCPE', largeAlias: 'PCOE-PCPE', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    4100011: {alias: 'POLE', largeAlias: 'POLE', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    12301: {alias: 'UDEC', largeAlias: 'UDEC', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    4000018: {alias: 'VOLT', largeAlias: 'VOLT', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'},
    410009: {alias: 'P. AUTÓNOMO', largeAlias: 'PARTIDO AUTÓNOMO', color: '#575757', bloque: '', cssClass: 'table__partido--recortes'}
}