import { select, event } from 'd3-selection';

export let tooltip = select('body')
    .append('div')
    .attr('class', 'tooltip-widget-madrid')
    .style("opacity", 0);

export function setTooltipPositionMap() {
    let mapa = document.getElementsByClassName('map')[0];
    let rect = mapa.getBoundingClientRect();

    let posicionHorizontal = event.clientX - rect.left > 140 ? event.clientX - rect.left > 200 ? 180 : 140 : 30;
    let posicionVertical = event.clientY - rect.top > 200 ? 175 : -25;

    return {posicionHorizontal, posicionVertical};
}

export function setTooltipPositionGeneral() {
    let posicionHorizontal = 45;
    let posicionVertical = -20;

    return {posicionHorizontal, posicionVertical};
}

export function setTooltipPositionGeneralPactos() {
    let posicionHorizontal = 45;
    let posicionVertical = 40;

    return {posicionHorizontal, posicionVertical};
}