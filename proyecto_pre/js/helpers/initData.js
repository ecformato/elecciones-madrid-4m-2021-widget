import '@babel/polyfill';
import 'whatwg-fetch';
import { apis, backUpApis } from './variables';
import { initEmptyData, setHemicycles, setMapValues, setPoliticalPacts, setResumeValues, setSuperPartiesVotes, setVsBar } from './drawData';
let auxGeneralData = [], auxLocalData = [];
//let hasData = true; //Variable de prueba para iniciar de una forma u otra | Quitar cuando hagamos pruebas más reales

async function initGeneralData(data) {
    let currentElection = {participation: data.places.CA13.participation, parties: data.places.CA13.results};

    //Pintado de barra indep. vs no indep.
    setVsBar(currentElection.parties);

    //Hemiciclos
    setHemicycles(currentElection.parties);

    // //Posibles pactos
    setPoliticalPacts(currentElection.parties);

    //Escaños por partido y comparativo con 2017
    setSuperPartiesVotes(currentElection.parties);
    
    //Escrutinio y participación
    let percentEscrutado2021 = currentElection.participation.par_percent_counted;
    let percentParticipacion2021 = currentElection.participation.par_percent_total_votes;
    setResumeValues(percentEscrutado2021, percentParticipacion2021);
}

async function initLocalData(data) {
    let localData = data;
    //Mapa municipal
    setMapValues(localData);
}

export async function initData() {
    Promise.all(apis.map(async function(api){
        return window.fetch(api).then( function(response){ return response.json(); })
    })).then(function(data){
        if((data[0].data.places.CA13.results.length > 0) && (data[1].data.length > 0)) {
            auxGeneralData = data[0].data;
            auxLocalData = data[1].data;  
            initGeneralData(auxGeneralData);
            initLocalData(auxLocalData);
        } else {
            initEmptyData();
        }     
    }).catch(function(error){
        console.log("Ha fallado la primera llamada por: " + error + " > Hacemos llamada a las URL que actúan de backup");

        Promise.all(backUpApis.map(async function(api){
            return window.fetch(api).then( function(response){ return response.json(); })
        })).then(function(data){
            if((data[0].data.places.CA13.results.length > 0) && (data[1].data.length > 0)) {
                auxGeneralData = data[0].data;
                auxLocalData = data[1].data;     
                initGeneralData(auxGeneralData);
                initLocalData(auxLocalData);
            } else {
                initEmptyData();
            }
        }).catch(function(error2){
            console.log("Falla el Plan B. Estamos perdidos por: " + error2);
            initEmptyData();
        });
    });
}