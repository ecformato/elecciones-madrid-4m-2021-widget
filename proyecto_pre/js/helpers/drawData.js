import { select, selectAll, event } from 'd3-selection';
import { transition as d3transition } from 'd3-transition';
import { arc as d3arc, pie as d3pie } from 'd3-shape';
import { superPartyBlockId } from './variables';
import { tooltip, setTooltipPositionMap, setTooltipPositionGeneral, setTooltipPositionGeneralPactos } from './tooltips';
import { applyCustomOrder, ordenacionTabla, getNumberWithThousandsPoint, isTablet } from './domLogic';

let superPartiesTable = document.getElementById('tableContent');
let initialRows = superPartiesTable.getElementsByClassName('table__row');

let mobileBig = isTablet() ? 50 : 45;

const transition = d3transition().duration(250);

/* INICIO - Uso de datos a nivel autonómico */
function setVsBar(data) {
    let izquierda = 0, 
    izquierdaPorc = 0, 
    derecha = 0, 
    derechaPorc = 0, 
    total = 136; //Cambiar a 136

    //Lógica de inserción de datos
    for(let i = 0; i < data.length; i++) {
        let partyId = data[i].res_party.par_meta_id;
        let bloque = superPartyBlockId[partyId] ? superPartyBlockId[partyId].bloque : 'error';

        if (bloque == 'izquierda') {
            izquierda += data[i].res_members;
            izquierdaPorc += data[i].res_percent_votes;
        } else if (bloque == 'derecha') {
            derecha += data[i].res_members;
            derechaPorc += data[i].res_percent_votes;
        }
    }

    let izqValue = (izquierda * 100) / total;
    let derValue = (derecha * 100) / total;

    //Pintado de información
    let vsIzquierda = isTablet() ? select("#vsIzquierdaResponsive") : select("#vsIzquierda");
    let vsDerecha = isTablet() ? select("#vsDerechaResponsive") : select("#vsDerecha");
    vsIzquierda.style('width',`${izqValue}%`);
    vsDerecha.style('width', `${derValue}%`);

    //Lógica de tooltips
    vsIzquierda
        .on('mouseenter', function() {
            tooltip.html(`
                <div class="tooltip-widget-madrid--general">
                    ${izquierda} escaños (${izquierdaPorc.toFixed(2).toString().replace('.',',')}% de votos)
                </div>
            `); 
        })
        .on('mouseover mousemove', function(){
            let posicionTooltip = setTooltipPositionGeneral();           

            tooltip.transition()
                .style('display', 'block')
                .style('opacity','0.9')
                .style("left", ""+ (event.pageX - posicionTooltip.posicionHorizontal) +"px")     
                .style("top", ""+ (event.pageY - posicionTooltip.posicionVertical) +"px");
        })
        .on('mouseout', function(){
            tooltip.transition()
                .style('display','none')
                .style('opacity', 0);
        });

    vsDerecha
        .on('mouseover mousemove', function(){
            let posicionTooltip = setTooltipPositionGeneral();               

            tooltip.html(`
                <div class="tooltip-widget-madrid--general">
                    ${derecha} escaños (${derechaPorc.toFixed(2).toString().replace('.',',')}% de votos)
                </div>
            `);

            tooltip.transition()
                .style('display', 'block')
                .style('opacity','0.9')
                .style("left", ""+ (event.pageX - posicionTooltip.posicionHorizontal) +"px")     
                .style("top", ""+ (event.pageY - posicionTooltip.posicionVertical) +"px");
        })
        .on('mouseout', function(){
            tooltip.transition()
                .style('display','none')
                .style('opacity', 0);
        });
}

function setResumeValues(escrutado2021, participacion2021) {
    document.getElementById('percentEscrutado').innerHTML = `${escrutado2021.toString().replace('.',',')}%`;
    document.getElementById('percentParticipacion2021').innerHTML = `${participacion2021.toString().replace('.',',')}%`;
    document.getElementById('percentParticipacion2019').innerHTML = `68,08%`;

    document.getElementById('barEscrutado').style.width = `${escrutado2021}%`;
    document.getElementById('barParticipacion2021').style.width = `${participacion2021}%`;
    document.getElementById('barParticipacion2019').style.width = `68.08%`;
}

function setPoliticalPacts(data) {
    let firstPact = 0; //PP + VOX
    let secondPact = 0; //PP + VOX + CS
    let thirdPact = 0; //PSOE + MM
    let fourthPact = 0; //PSOE + MM + PODEMOS
    let fifthPact = 0; //PSOE + MM + CS
    let sixthPact = 0; //PSOE + MM + PODEMOS + CS
    let total = 136; //Cambiar a 136

    //Lógica de recogida total de escaños
    setEachPact();
    
    //Lógica de tooltips
    let pactosPoliticos = selectAll('.pacto__bar');
    pactosPoliticos
        .on('mouseenter', function() {
            let datoPacto = '';

            if(this.classList.contains('pacto__bar--first')){
                datoPacto = firstPact;
            } else if (this.classList.contains('pacto__bar--second')) {
                datoPacto = secondPact;
            } else if (this.classList.contains('pacto__bar--third')) {
                datoPacto = thirdPact;
            } else if (this.classList.contains('pacto__bar--fourth')) {
                datoPacto = fourthPact;
            } else if (this.classList.contains('pacto__bar--fifth')) {
                datoPacto = fifthPact;
            } else if (this.classList.contains('pacto__bar--sixth')) {
                datoPacto = sixthPact;
            }

            tooltip.html(`
                <div class="tooltip-widget-madrid--general">
                    <span style='text-align: center;'>${datoPacto} escaños</span>
                </div>
            `);
        })
        .on('mouseover mousemove', function(){           
            //Tooltip
            let posicionTooltip = setTooltipPositionGeneralPactos();           

            tooltip.transition()
                .style('display', 'block')
                .style('opacity','0.9')
                .style("left", ""+ (event.pageX - posicionTooltip.posicionHorizontal) +"px")     
                .style("top", ""+ (event.pageY - posicionTooltip.posicionVertical) +"px");
        })
        .on('mouseout', function(){
            tooltip.transition()
                .style('display', 'none')
                .style('opacity','0');
        });

    /* Helper interno */
    function setEachPact(){
        for(let i = 0; i < data.length; i++) {
            if(data[i].res_party.par_meta_id == 40) { //PSOE
                let datosPsoe = data[i].res_members;
                thirdPact += datosPsoe;
                fourthPact += datosPsoe;
                fifthPact += datosPsoe;
                sixthPact += datosPsoe;
    
                //Pintado
                let psoeBars = document.getElementsByClassName('pacto__value--psoe');
                for(let i = 0; i < psoeBars.length; i++){
                    psoeBars[i].style.width = `${(datosPsoe * 100) / total}%`;
                }
            } else if (data[i].res_party.par_meta_id == 38) { //PP
                let datosPP = data[i].res_members;
                firstPact += datosPP;
                secondPact += datosPP;
    
                //Pintado
                let ppBars = document.getElementsByClassName('pacto__value--pp');
                for(let i = 0; i < ppBars.length; i++){
                    ppBars[i].style.width = `${(datosPP * 100) / total}%`;
                }
            } else if (data[i].res_party.par_meta_id == 1000091) { //MÁS MADRID
                let datosMM = data[i].res_members;
                thirdPact += datosMM;
                fourthPact += datosMM;
                fifthPact += datosMM;
                sixthPact += datosMM;
    
                //Pintado
                let mmBars = document.getElementsByClassName('pacto__value--masmadrid');
                for(let i = 0; i < mmBars.length; i++){
                    mmBars[i].style.width = `${(datosMM * 100) / total}%`;
                }
            } else if (data[i].res_party.par_meta_id == 64) { //UP
                let datosUP = data[i].res_members;
                fourthPact += datosUP;
                sixthPact += datosUP;
    
                //Pintado
                let podemBars = document.getElementsByClassName('pacto__value--podemos');
                for(let i = 0; i < podemBars.length; i++){
                    podemBars[i].style.width = `${(datosUP * 100) / total}%`;
                }
            } else if (data[i].res_party.par_meta_id == 10) { //CS
                let datosCs = data[i].res_members;
                secondPact += datosCs;
                fifthPact += datosCs;
                sixthPact += datosCs;

                //Pintado
                let csBars = document.getElementsByClassName('pacto__value--cs');
                for(let i = 0; i < csBars.length; i++){
                    csBars[i].style.width = `${(datosCs * 100) / total}%`;
                }
            } else if (data[i].res_party.par_meta_id == 52) { //VOX
                let datosVOX = data[i].res_members;
                firstPact += datosVOX;
                secondPact += datosVOX;                
    
                let voxBars = document.getElementsByClassName('pacto__value--vox');
                for(let i = 0; i < voxBars.length; i++){
                    voxBars[i].style.width = `${(datosVOX * 100) / total}%`;
                }
            } 
        }
    }    
}

function setSuperPartiesVotes(data){
    let partidosConEscano = [];
    for(let i = 0; i < data.length; i++) {
        if(data[i].res_members > 0 || data[i].res_members_previous > 0){
            partidosConEscano.push(data[i]);
        }
    }

    //Reordenación del array (mutación ya que es un array interno) a partir de 'res_members'
    partidosConEscano.sort(ordenacionTabla);
    
    //Comprobar si hay más de 6 partidos con escaño para crear nuevas filas en el DOM
    let partidosConEscanosTotal = partidosConEscano.length;
    let initialRowsTotal = initialRows.length;

    if(partidosConEscanosTotal > initialRowsTotal){
        while(initialRowsTotal < partidosConEscanosTotal){
            let cloneNode = initialRows[0].cloneNode(true);
            superPartiesTable.appendChild(cloneNode);
            initialRowsTotal++;
        }
    }

    //Pasamos a nutrir las filas con los datos
    for(let i = 0; i < partidosConEscanosTotal; i++){
        let partido = partidosConEscano[i];
        let fila = initialRows[i];

        //Pintar círculo > Clase CSS
        fila.getElementsByClassName('table__partido')[0].classList.add(superPartyBlockId[partido.res_party.par_meta_id].cssClass);

        //Pintar nombre
        fila.getElementsByClassName('table__partido')[0].textContent = superPartyBlockId[partido.res_party.par_meta_id].alias;

        //Pintar total de votos
        fila.getElementsByClassName('table__value')[0].textContent = `${getNumberWithThousandsPoint(partido.res_votes)} (${Math.round(partido.res_percent_votes)}%)`;

        //Pintar escaños
        fila.getElementsByClassName('table__value')[1].textContent = partido.res_members;

        //Pintar escaños de 2019
        fila.getElementsByClassName('table__value')[2].textContent = partido.res_members_previous;
        
        //Pintar subida o bajada
        let imgComparative = fila.getElementsByClassName('table__value--icon')[0];
        
        if(partido.res_members > partido.res_members_previous){
            imgComparative.src = 'https://www.ecestaticos.com/file/59138a0f8122a460ea50930c7481d5b2/1612914662-icono-sube.svg';
        } else if (partido.res_members < partido.res_members_previous){
            imgComparative.src = 'https://www.ecestaticos.com/file/ba9ab8ff150fae6aa699cf903c1f610d/1612914683-icono-baja.svg';
        } else {
            imgComparative.src = 'https://www.ecestaticos.com/file/8fcce682e23eb9fa9037de230a36760d/1612914710-icono-igual.svg';
        }        
    }
}

function setHemicycles(data) {
    /* Validar información con Redacción > ID para pintar al PDCat, que en 2017 integraba la lista de JxCAT */
    let width = document.getElementById('hemiciclo').clientWidth,
        height = document.getElementById('hemiciclo').clientHeight;

    let svg = select('#hemiciclo')
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height + ")");

    // Orden customizado para hemiciclos
    const orderIWant = [64, 1000091, 40, 10, 38, 52];
    let newData = applyCustomOrder(data, orderIWant);
    
    setHemicycle('2021');
    setHemicycle('2019');

    function setHemicycle(year) {
        let innerHeight = year == '2021' ? height : height / 2;
        let minusRadius = year == '2021' ? mobileBig : 30;
        let innerData = year == '2021' ? 'res_members' : 'res_members_previous';
        let innerCssClass = year == '2021' ? 'arc' : 'small-arc';
        let innerOpacity = year == '2021' ? .8 : .6;

        //Inicio lógica
        let radius = innerHeight;

        let arc = d3arc()
            .outerRadius(radius)
            .innerRadius(radius - minusRadius);

        let pie = d3pie()
            .value(function(d) {
                return d[innerData];                         
            })
            .sort(null)
            .startAngle(-1 * Math.PI / 2)
            .endAngle(Math.PI / 2);
            
        let piedData = pie(newData);
        
        let slice = svg.selectAll(`${innerCssClass}`)
            .data(piedData)
            .enter()
            .append("g")
            .attr("class", function(d) {
                return `${innerCssClass} arc-${superPartyBlockId[d.data.res_party.par_meta_id] ? superPartyBlockId[d.data.res_party.par_meta_id].alias : d.data.res_party.par_alias}`;
            });

        slice
            .append("path")            
            .style("fill", function(d) {
                return superPartyBlockId[d.data.res_party.par_meta_id] ? superPartyBlockId[d.data.res_party.par_meta_id].color : 'green';
            })
            .style("opacity", innerOpacity)
            .attr("d", arc)
            .style('stroke', ' #fff')
            .style('stroke-width', '1.5px')
            .on('mouseenter', function(d,i,e) {
                tooltip.html(`
                    <div class="tooltip-widget-madrid--general">
                        <span style="display: block;">${superPartyBlockId[d.data.res_party.par_meta_id] ? superPartyBlockId[d.data.res_party.par_meta_id].alias : d.data.res_party.par_alias}</span>
                        <span style="display: block;">Escaños 2021: ${d.data.res_members}</span>
                        <span style="display: block;">Escaños 2019: ${d.data.res_members_previous}</span>
                    </div>
                `);
            })
            .on('mouseover', function(d,i,e){
                //Opacities
                let cssClass = e[i].parentElement.classList.item(1);
                let elementsWithClass = document.getElementsByClassName(cssClass);

                for(let i = 0; i < elementsWithClass.length; i++){
                    elementsWithClass[i].firstChild.style.opacity = 1;
                }

                //Tooltip
                let posicionTooltip = setTooltipPositionGeneral();               

                tooltip.transition()
                    .style('display', 'block')
                    .style('opacity','0.9')
                    .style("left", ""+ (event.pageX - posicionTooltip.posicionHorizontal) +"px")     
                    .style("top", ""+ (event.pageY - posicionTooltip.posicionVertical) +"px");
            })
            .on('mouseout', function(d,i,e){
                //Opacities
                let cssClass = e[i].parentElement.classList.item(1);
                let elementsWithClass = document.getElementsByClassName(cssClass);

                elementsWithClass[0].firstChild.style.opacity = .8;
                elementsWithClass[1].firstChild.style.opacity = .6;

                tooltip.transition()
                    .style('display','none')
                    .style('opacity', 0);
            });
    }
}
/* FINAL */

/* INICIO - Uso de datos a nivel municipal */
function setMapValues(data) {
    /*
    * Lógica de pintado > Bucle para obtener la fuerza con más escaños/porcentaje de votos obtenidos
    */
    selectAll('.mun')
        .style('stroke', '#fff')
        .style('stroke-width', '0.75px')
        .style('fill', function() {
            let municipio = data.filter(item => { return item.id == this.id.substr(0,5)})[0];
            
            if(municipio) {
                let primeraFuerza = municipio.parties[0];
            
                if(primeraFuerza) {
                    let idPartido = primeraFuerza.par_meta_id;
                    return superPartyBlockId[idPartido].color;
                } else {
                    return '#e1e1e1';
                } 
            } else {
                return '#e1e1e1';
            }
                       
        })
        .on('mouseenter', function() {
            //Recogemos y manipulamos los datos
            let nombreMunicipio = this.id.substr(5,);
            let datosMunicipio = data.filter(item => { return item.id == this.id.substr(0,5)})[0];
            
            if(datosMunicipio && datosMunicipio.parties.length > 0) {
                //Primera fuerza
                let primeraFuerzaPartido = superPartyBlockId[datosMunicipio.parties[0].par_meta_id].alias;
                let primeraFuerzaPorc = datosMunicipio.parties[0].par_percent;
                //Segunda fuerza
                let segundaFuerzaPartido = superPartyBlockId[datosMunicipio.parties[1].par_meta_id].alias;
                let segundaFuerzaPorc = datosMunicipio.parties[1].par_percent;

                //Porcentaje de bloques
                let partidosConVotos = datosMunicipio.parties;
                let bloqueIzqPorc = 0, bloqueDerPorc = 0, bloqueRestoPorc = 0;

                for(let i = 0; i < partidosConVotos.length; i++){
                    let bloqueIdeologico = superPartyBlockId[partidosConVotos[i].par_meta_id] ? superPartyBlockId[partidosConVotos[i].par_meta_id].bloque : 'error';

                    if (bloqueIdeologico == 'izquierda') {
                        bloqueIzqPorc += partidosConVotos[i].par_percent;
                    } else if (bloqueIdeologico == 'derecha') {
                        bloqueDerPorc += partidosConVotos[i].par_percent;
                    } else {
                        bloqueRestoPorc += partidosConVotos[i].par_percent;
                    }
                }

                //Regla de tres para ancho de barra en bloques ideológicos
                const ANCHOMAX = 135;
                let anchoIzq = (bloqueIzqPorc * ANCHOMAX) / 100, 
                    anchoDer = (bloqueDerPorc * ANCHOMAX) / 100, 
                    anchoResto = (bloqueRestoPorc * ANCHOMAX) / 100;

                //Pintamos el HTML
                tooltip.html(`
                    <div class="tooltip-widget-madrid--mun">                    
                        <div class="widget__title">${nombreMunicipio}</div>
                        <div class="b-fuerzas">
                            <div class="sb-fuerza">
                                <span>1ª fuerza</span>
                                <span>${primeraFuerzaPartido} (${primeraFuerzaPorc.toFixed(1).toString().replace('.',',')}%)</span>
                            </div>
                            <div class="sb-fuerza">
                                <span>2ª fuerza</span>
                                <span>${segundaFuerzaPartido} (${segundaFuerzaPorc.toFixed(1).toString().replace('.',',')}%)</span>
                            </div>
                        </div>
                        <div class="bloques">
                            <div class="bloques__title">Bloques</div>
                            <div class="bloques__items">
                                <div class="bloque__item">
                                    <span>Izda.</span>
                                    <div class="bloque__bar" style="width: ${anchoIzq}px; background: #fbdd49;"></div>
                                    <span>${bloqueIzqPorc.toFixed(1).toString().replace('.',',')}%</span>
                                </div>
                                <div class="bloque__item">
                                    <span>Dcha.</span>
                                    <div class="bloque__bar" style="width: ${anchoDer}px; background: #3c4554;"></div>
                                    <span>${bloqueDerPorc.toFixed(1).toString().replace('.',',')}%</span>
                                </div>
                                <div class="bloque__item">
                                    <span>Otros</span>
                                    <div class="bloque__bar" style="width: ${anchoResto}px; background: #cdcdcd; "></div>
                                    <span>${bloqueRestoPorc.toFixed(1).toString().replace('.',',')}%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                `);
            } else {
                //Pintamos el HTML
                tooltip.html(`
                    <div class="tooltip-widget-madrid--mun">                    
                        <div class="widget__title">${nombreMunicipio}</div>
                        <p style="font-size: 12px;">Sin datos</p>
                    </div>
                `);
            }           
        })
        .on('mouseover mousemove', function() {
            //Ubicamos el tooltip
            let posicionTooltip = setTooltipPositionMap();

            tooltip.transition()
                .style('display', 'block')
                .style('opacity','1')
                .style("left", ""+ (event.pageX - posicionTooltip.posicionHorizontal) +"px")     
                .style("top", ""+ (event.pageY - posicionTooltip.posicionVertical) +"px");
        })
        .on('mouseout', function() {
            tooltip.transition()
                .style('display', 'none')
                .style('opacity','0');
        });
        
}
/* FINAL */

//////
/// WIDGET VACÍO ----> Dibujar hemiciclo y tabla de partidos || Mapa, pactos y barras de progreso ya vienen pintadas por HTML y CSS
//////
function initEmptyData() { 
    //Hemiciclo
    let width = document.getElementById('hemiciclo').clientWidth,
        height = document.getElementById('hemiciclo').clientHeight;
        
    drawEmptyHemicycle(mobileBig, 1);
    drawEmptyHemicycle(30, .65, true);

    //Tabla
    drawEmptyTable(initialRows);   
    
    /* 
    * Helpers internos 
    */
    function drawEmptyHemicycle(minusRadius, opacity, is2017 = false) {
        let radius = is2017 ? height / 2 : height;

        let arc = d3arc()
            .outerRadius(radius)
            .innerRadius(radius - minusRadius)
            .startAngle(-1 * Math.PI / 2)
            .endAngle(Math.PI / 2);

        let svg = select('#hemiciclo')
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height + ")");
        
        svg.append("path")
            .attr("d", arc)
            .style('opacity', opacity)
            .style("fill", '#c6c6c6');
    }

    function drawEmptyTable(initialRows) {
        for(let i = 0; i < initialRows.length; i++) {
            let row = initialRows[i];
            row.getElementsByClassName('table__partido')[0].textContent = '-';
            row.getElementsByClassName('table__value')[0].textContent = '-';
            row.getElementsByClassName('table__value')[2].textContent = '-';
        }
    }
}

export {
    initEmptyData,
    setSuperPartiesVotes,
    setVsBar,
    setResumeValues,
    setHemicycles,
    setPoliticalPacts,
    setMapValues
}